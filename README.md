# ringwooddementia.org.uk

The https://ringwooddementia.org.uk site.

-   Built using gridsome
-   Hosted using gitlab pages
-   CDN + SSL via cloudflare

## Development

1. `yarn install` - install packages
2. `gridsome develop` to start a local dev server at `http://localhost:8080`
3. Happy coding 🎉🙌
