---
id: contact-details
---
## Contact Us

### Sue Scott

Tel [`01202 986179`](tel:01202986179) or email [`scottsusan30@gmail.com`](matilto:scottsusan30@gmail.com)


---

Facebook - [`Ringwood & Bransgore Dementia Action Group`](https://www.facebook.com/ringwooddementia/)

Twitter - [`@rbdementia`](https://twitter.com/rbdementia)
