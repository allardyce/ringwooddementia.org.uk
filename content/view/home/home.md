---
id: home
---

## Who we are

We are a volunteer group committed to developing Ringwood, Bransgore and the surrounding area as a Dementia Friendly Community.

We are keen to improve services through awareness and understanding within the community. As a volunteer action group we are committed to creating an environment that offers the best possible opportunity for people to live well with dementia.