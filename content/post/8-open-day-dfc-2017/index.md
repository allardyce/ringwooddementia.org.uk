---
slug: open-day-dfc-2017
title: Public Open Afternoon
order: 8
thumb: ./open-day-dfc-2017.thumb.jpg
description: In celebration of 1,000 Dementia Friends in our local area
image: ./open-day-dfc-2017.jpg
archived: true
---
