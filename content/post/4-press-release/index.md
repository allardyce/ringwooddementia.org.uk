---
slug: press-release
title: Recent Press Release
order: 4
thumb: ./press-release-1.thumb.jpg
description: Town Councilors lead the way to make Ringwood and Bransgore a Dementia Friendly Community.
archived: true
---

## Ringwood and Bransgore to become Dementia Friendly Communities

Creating Dementia Friendly Communities is important. Over the next 20 years the number of people living with dementia, especially those with mild to moderate dementia, is expected to increase dramatically.

By becoming Dementia Friends, Councillors of Ringwood and Bransgore, Police, local people and community groups have taken the first steps towards making both communities Dementia Friendly. So far, over a hundred people have attended a 45 minute session that gives them an awareness and understanding of those who live with dementia. It is hugely reassuring for people with dementia, to feel able to go out and about and continue to do the everyday things most people take for granted, knowing that their needs within the community will be understood and supported.

Dementia Friendly High Streets are being set up by Dementia Action Groups throughout the country. Businesses, services, and organisations are involved, and able to respond to customers by creating an environment that offers the best possible opportunity for people to live well with dementia. **This will also benefit the disabled, those with mental health concerns, as well as older people living within the community.**

Ringwood and Bransgore Dementia Action Group invites you to become a Dementia Friend, so that people living with dementia can feel confident and able to continue their lives, their interests and activities, because they live in a Dementia Friendly Community.

To become a Dementia Friend, or if you wish to join the Action Group please contact

**Sue Scott** - Tel `01202 986179`, Email `sue@walking-on-sunshine.com`

or

**Krista Allardyce** - `Tel 01425 483117`, Email `kristaallardyce@hotmail.com`

Free information sessions will be held regularly at the Lesley Errington Hall, Stillwater Park, North Poulner Road. Ringwood. BH24 3JZ