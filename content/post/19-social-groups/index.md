---
slug: social-groups
title: Local and Social Groups
order: 19
thumb: ./local-and-social-groups.thumb.png
description: Helping people to engage in community life
image: ./local-and-social-groups.png
---
