---
slug: communities
title: Dementia Friendly Communities
order: 20
thumb: ./communities.thumb.png
description: A picture paints a thousand words. What does becoming dementia friendly mean?
image: ./communities.png
---