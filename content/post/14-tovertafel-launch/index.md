---
slug: tovertafel-launch
title: Tovertafel Launch
order: 14
thumb: ./tovertafel-launch.thumb.jpg
description: The launch event
archived: true
---

## Tovertafel Launch

Ringwood & Bransgore Dementia Action Group, Care UK Ferndown Manor and Tilney Financial Planning Ltd have partnered up to purchase a Tovertafel or “Magic Table”.  This will be donated to the Trinity United Church and will be based in the Trinity Centre, for hire to all those in the community who are living with dementia.

Tovertafel arrived in the UK in 2015, and was named as a finalist in the 8th National Dementia Care Awards 2017.  It was developed in the Netherlands and there are already over 2,000 Tovertafels installed in care homes in the Benelux Countries.  Its name comes from someone with dementia, who announced “This is a magic table!”

The Tovertafel is a series of interactive light animations and games, which are projected onto a table. It is a social activity that motivates people to instinctively participate, stimulating both physical and cognitive activity and is designed for people with mid - to late stage dementia.

Sue Scott says ”We were delighted when Amanda Cook from Tilney got in touch to say she was looking for help to raise funds to buy a Tovertafel.  The Trinity Day Care Centre was an obvious choice and it was lovely to see the reactions of the day centre attendees when we saw the demonstration.  We welcome local residents and companies to join us in our efforts to reach our target.  Do keep an eye out for our crowdfunding campaign and also watch out for the 'biathlon challenge’ during February, in the Furlong Centre to raise funds for this project.”

The Tovertafel is now installed and ready for demonstration.  Ringwood Town Mayor, Councillor Tim Ward will attend the Launch Event on <span class="text-red">**Saturday 12th May, 1pm - 5pm**</span> - all are welcome.

The event will be informative, fun and educational.  There will be an opportunity to hear from different speakers on subjects including, living well with dementia and planning for later life.  There will be interesting stands to browse and a glass of bubbly when we officially launch the Tovertafel and announce the winners of the raffle! 

The Dementia Friendly fundraising project includes Ringwood and its surrounding communities.
		

[https://ringwooddementia.org.uk](https://ringwooddementia.org.uk) | 
[http://www.tilney.co.uk](www.tilney.co.uk) | [http://shift8.co.uk](http://shift8.co.uk)
