---
slug: latest-news
order: 1
title: Latest News
thumb: ./news-1.thumb.jpg
description: Ringwood & Bransgore communities are gathering momentum
archived: true
---

## Dementia Friendly Ringwood & Bransgore communities are gathering momentum

Local people are responding to find ways of improving the quality of life for people living with dementia and their carers. Since February, there are now over 342 local people who have become Dementia Friends, raising awareness and understanding throughout the community. Businesses and services, local shops and organisations are forming action plans and committing to become Dementia Friendly. Small changes to attitude and customer service, along with practical ideas, will help and encourage people with dementia to engage in community life. Whether shopping, finding transport or simply being able to continue to pursue interests and activities with confidence - life will be made easier.

As a volunteer action group we are committed to creating an environment that offers the best possible opportunity for people to live well with dementia.