---
slug: encouraging-and-enabling
title: Encouraging & Enabling
order: 7
thumb: ./launch-event-names.thumb.jpg
description: Enabling People with Dementia to Engage in the Community
image: ./launch-event-names.jpg
archived: true
---