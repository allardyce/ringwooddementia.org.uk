---
slug: dementia-friendly-allotment
title: A Dementia Friendly Allotment
order: 24
thumb: ./dementia-friendly-allotment.thumb.jpg
description: Fresh growth is already appearing with a nice show of Crocuses and Daffodils
archived: false
---

## A Dementia Friendly Allotment

It’s time to get outside and start the new season with weeding and sowing at our allotment garden in Ringwood. Fresh growth is already appearing with a nice show of Crocuses and Daffodils by the entrance.

Since last year, we have cleared the site, erected a tool shed, a shelter and several raised beds. Grass seed was sown on the pathway last autumn and overwintering crops such as spinach, leeks, winter cabbages and leafy salads were grown under netting.

This year we plan to build a large fruit cage at the rear of the plot, which will house raspberries, blackcurrants, gooseberries and cabbage family plants.

There is a good supply of tools available for those who want to help and spend some pleasant time at the allotment. The aim is to have social sessions on Tuesday afternoons and Thursday mornings at the allotment starting in April.

Low level exercise, such as gardening helps improve strength and mobility and reduces anxiety and stress levels. Being outside in the sunshine and working with nature also brings the feel good factor.

We look forward to hearing from you.

### Contact

`Sue 07775 908330`

![The Allotment](./dementia-friendly-allotment.jpg)
