---
slug: music-and-memory
title: Working to Become Dementia Friendly
order: 5
thumb: ./music-and-memory.thumb.png
description: Our Music and Memory Project is underway
archived: true
---

## Working to Become Dementia Friendly

The local Dementia Friendly Action Group is busy, as the community embraces the concept of Dementia Friendly Communities.

Our Music and Memory Project is underway, thanks to funding from the Ringwood & Fordingbridge Lions Club, and the team of Ringwood school pupils who gained third place in the ‘Lions Den’ competition.

Four Care Homes, Bickerley Green, St Elmo, Foxes Moon at St Ives and Woodside Lodge in Bransgore have been selected as a pilot project to bring personal playlists to people with dementia. Listen to a song, a tune, and memories will also form, of time, or place. Music has shown to be effective in reducing anxiety and agitation, improving ability to understand and think, and can help a sense of identity and independence – even in people with advanced dementia.

We have presented an information pack, memory sticks of tunes, and the inspirational documentary, Alive Inside, with a Music Kit that includes MP3 player and headphones that will benefit two residents and their families in each Care Home. Scotland has pioneered the Playlist for Life and has an impressive website [www.playlistforlife.org.uk](http://www.playlistforlife.org.uk) which is accessible to all. We hope to see the project becoming mainstream for many more people.

A heartening 800 local people have become Dementia Friends, raising awareness that helps people with dementia to feel safe and confident in the community. We have established a growing list of social groups in the area for people with dementia and their carers to continue, to pursue interests old and new. Health, hobbies and relaxation are key to a good life, and we are keen to hear from groups who may invite further ideas to expand the list which can be found on our website [http://www.ringwooddementia.org.uk](https://ringwooddementia.org.uk).

We would like to say thank-you to all those who are providing us with the funding and encouragement to create ideas and opportunities that only a strong sense of local community can offer.

<p style="text-align: right; color: #888">For more details contact Krista 01425 483117 : Sue 07775 908330