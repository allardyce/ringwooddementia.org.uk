---
slug: allotment-garden
title: New Project for Dementia Friendly
order: 22
thumb: ./allotment-garden.thumb.jpg
description: Reconnect with food, nature and community.
---

## New Project for Dementia Friendly

Spring arrives in the garden with snowdrops and daffodils, the promise of warmer days, fresh air and sunshine, it is good for the soul. Our new fundraising venture is to create an allotment project that enables older people to socialise while gardening together.

Work is underway, and we are looking for people to help create the vision. We will need help with a shed, seating, raised beds and tools to begin. At the allotment garden we have the benefits of simplicity, a sanctuary to reconnect with food, nature and community.

Research suggests that allotments and gardens provide a low-impact form of exercise that activates a broad range of muscle groups. Regular exercise, helps one to remain supple, improves strength, endurance and mobility. Reduce stress levels and anxiety, and slow cognitive decline can be delayed. Being outside in the sun for even a relativity short amount of time stimulates the production of vitamin D, which strengthen bones and reduce the risk of depression.

Birdsong, company and bring a thermos flask … we look forward to hearing from you.

Contact: Sue `07775 908330`

![Allotment Garden](./allotment-garden.jpg)
