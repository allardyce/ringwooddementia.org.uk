---
slug: public-open-event
title: Celebrating 1,000 Dementia Friends
order: 12
thumb: ./public-open-event.thumb.png
description: Public Open Event
archived: true
---

## A Thousand Local People Have Become Dementia Friends

Ringwood and Bransgore Dementia Action Group is pleased to announce that one thousand people have become Dementia Friends in the local area!  We will be celebrating the event, with a Public Open Afternoon on March 16th at the Trinity Centre in Ringwood and all are welcome.  The afternoon is designed to inform and inspire.

In the last 5 years, Dementia Friendly Communities have changed the landscape of dementia with new technologies and innovation - including therapeutic robots in Care Homes!  Many new ideas and projects are now emerging.  On the day, there will be information stands, opportunities to try some taster sessions and a talk about nutrition at 12.30pm.  There will be musical entertainment by Don Taylor and refreshments courtesy of Sainsbury’s.

We are now looking at local sport clubs, starting with Walking Football and Cage Cricket, in order to help more people with dementia, to continue leading active lives.  Our volunteers are teaming up with Bransgore Cricket Club and Bournemouth University to initiate this project and you will find further information at the event.

Meanwhile, the Music and Memory pilot project continues in four local Care Homes with excellent results so far.  We hope to see the project becoming mainstream for many more people.

The list of Dementia Friendly social groups continues to expand with more organisations offering leisure pursuits for a healthy lifestyle.  Added to the list is a brand new Memory Lane & Carers Café in Bransgore at the Community Church.  In Ringwood, the Wednesday Morning Coffee Social now meets in the Meeting House.

**For further details Tel: Sue  07775 908330  :  Krista  01425 483117**