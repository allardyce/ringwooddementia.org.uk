---
slug: more-news
title: More News
order: 2
thumb: ./news-1.thumb.jpg
description: Ringwood & Bransgore communities are gathering momentum
archived: true
---

## Dementia Friendly Ringwood and Bransgore Communities

The local action team would like to extend thanks to all who supported the Dementia Friendly Ringwood event at Greyfriars Community Centre. It proved to be an informative and imaginative day of sharing resources and experience.

The local people are responding to find ways of improving the quality of life for people living with dementia. In Ringwood and Bransgore, 321 people have become Dementia Friends since February. This raises the levels of awareness and understanding in the community, so that people with a diagnosis of dementia feel supported and are encouraged to continue, to lead active and independent lives.

Businesses and services, local shops and organisations are the next stage in the plan to help create a Dementia Friendly environment. They are signing up to the Hampshire Dementia Action Alliance, forming action plans and committing to become Dementia Friendly. Small changes to attitude and customer service along with practical ideas, will make life easier for people with dementia and their carers.

We look forward to the Launch event in the Autumn, to celebrate those in the community who will be the first to qualify for the status of “Working to become Dementia Friendly” in both Ringwood and Bransgore.