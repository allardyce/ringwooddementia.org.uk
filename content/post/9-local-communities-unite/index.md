---
slug: local-communities-unite
title: Celebration News
order: 9
thumb: ./local-communities-unite.thumb.jpg
description: Local Communities Unite in Becoming Dementia Friendly
archived: true
---

## Local Communities Unite in Becoming Dementia Friendly

A celebration with a difference has recently taken place at Greyfriars Community Centre in Ringwood - the Launch of Dementia Friendly Ringwood & Bransgore. This recognised the very first local businesses and organisations to join the Dementia Friends social movement and the Hampshire Dementia Action Alliance, responding to the needs of people with dementia.
					
The Mayor of Ringwood, Town Councillor Thierry, and the Chairman of Bransgore Parish Council, Mike Manley, made the presentations both echoing the same message "We can all make a difference".

Dementia Friends is a national initiative of the Cabinet and the Alzheimer's Society in response to the Prime Minister's challenge on dementia. Its aim is simple..... to increase dementia awareness and understanding and to change the way the nation thinks, talks and acts about dementia.

An impressive 350 local people have become Dementia Friends, and the local businesses and organisations are now helping to encourage and enable people with dementia to engage in the community. Whether shopping, finding transport or simply being able to continue to pursue interests and activities with confidence, life will be made easier.

The first local members of Dementia Friendly Hampshire will prominently display dementia friendly welcome stickers in premises throughout the communities of Ringwood and Bransgore. More will follow. Look for the three forget-me-nots and the understanding, supportive people who have decided to act and make a difference!
