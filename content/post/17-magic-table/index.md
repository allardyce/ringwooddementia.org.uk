---
slug: magic-table
title: Magic Table at Trinity Centre
order: 17
thumb: ./magic-table.thumb.jpg
description: Social and energetic activity play
---

## Magic Table at Trinity Centre

Residents of Bickerley Green Care Home, enjoy visiting the Magic Table at the Trinity Centre in Ringwood.  Activity co-ordinator Tracey Cotterill takes between 2-6 residents along with carers or relatives for support.  Colourful, sound effects and interactive light animations projected onto the table, motivate people to participate instinctively, they can touch flowers, pop bubbles, catch fish and have fun!

Tracey quotes: “Even residents who are initially shy or hesitant are fascinated by the moving images on the table.  There are many positive reactions with lots of laughter and oohs and aahs and they physically engage with the activity.  We particularly enjoy the leaves and ladybirds, and the paint balls that build up a painting.  The fee of £6.65 for the room hire per hour is excellent value enabling me to visit regularly with different residents who have varied need and abilities.”

The Tovertafel also provides social and energetic activity play with interactive light games, for people with learning disabilities.

To make a booking contact : Tel. 01425 461440  :  [contact@trinityringwood.co.uk](mailto:contact@trinityringwood.co.uk)

![Magic Table](./magic-table.jpg)
