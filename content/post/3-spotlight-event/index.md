---
slug: spotlight-event
title: Spotlight Event
order: 3
thumb: ./spotlight-event.thumb.jpg
description: Visit our Spotlight Event at Greyfriars Community Centre on Friday 22nd May
image: ./spotlight-event.jpg
archived: true
---
