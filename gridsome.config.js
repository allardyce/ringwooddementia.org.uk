// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
    siteUrl: 'https://ringwooddementia.org.uk',

    siteName: 'Dementia Action Group',

    description:
        'A volunteer group committed to developing Ringwood, Bransgore and the surrounding area as a Dementia Friendly Community.',

    outputDir: 'public',

    plugins: [
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'Post',
                path: './content/post/**/*.md',
            },
        },
        {
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'View',
                path: './content/view/**/*.md',
            },
        },
        {
            use: '@gridsome/plugin-google-analytics',
            options: {
                id: 'UA-61944834-1',
            },
        },
        {
            use: '@gridsome/plugin-sitemap',
        },
    ],

    templates: {
        Post: '/post/:slug',
    },

    transformers: {
        remark: {
            slug: false,
        },
    },
};
